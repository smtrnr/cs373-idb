import React, { useEffect, useState } from "react";
import { Table } from 'react-bootstrap';
import { Link, useHistory } from 'react-router-dom';

function ArtistInstance(props) {
    //const data = props.location.state.data;


    const history = useHistory();
    const routeChange = (pathName, rowData) =>{
      let path = pathName;
      history.push({
        pathname: path,
        state: { data: rowData},
      });
    }

    const [state, setState] = useState(props.location.state.data);
      useEffect(() => {
        fetch('/api/artists?id=' + state.artist_id).then(res => res.json()).then(data => {
          setState(data);
        });
      }, []);
    //<li><a href={item["album_spotify_url"]}>{item["album_name"]}</a></li>
    let album_list = [];
    if ("artist_albums" in state) {
    album_list = state.artist_albums.map(item => (
        <li style={{color: "blue"}} onClick={() => routeChange("/albumInstance", item)} >{item["album_name"]}</li>
    ));
    }
    //<li><a href={item["track_spotify_url"]}>{item["track_name"]}</a></li>
    let track_list = []
    if ("artist_tracks" in state) {
    track_list = state.artist_tracks.map(item => (
        <li style={{color: "blue"}} onClick={() => routeChange("/trackInstance", item)} >{item["track_name"]}</li>
    ));
    }
    let genre_list = state.artist_genres.map(item => (
        <li>{item}</li>
    ));
    return (
    <React.Fragment>
    <div>
        <a href={state.artist_spotify_url}> <h1>{state.artist_name}</h1> </a>
        <img src={state.artist_image_url} style={{width:"200px"}} />
        <br />
        <h5>Genres:</h5>
        <ul>
            {genre_list}
        </ul>
    </div>
    <div style={{float: "left", width: "50%"}}>
        <h3>Albums</h3>
        <ul>
            {album_list}
        </ul>
    </div>
    <div style={{float: "right", width: "50%"}}>
        <h3>Tracks</h3>
        <ul>
            {track_list}
        </ul>
    </div>
    </React.Fragment>
  );
}

export default ArtistInstance;
